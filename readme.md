## Huawei Honorcup Marathon @ Codeforces (https://codeforces.com/contest/1052/problem/C)
## Eighth place solution, team "now_you_see"
## Pavel Blinov (blinoff.pavel@gmail.com), Data Scientist at Yandex, Moscow, Russia


### Directory structure


```bash
+--blood-pressure-estimation-data
    +--readme.md
    +--main.ipynb
    +--peakdetect.py
    +--data_train
       +--subj3log127.csv
       +--subj3log128.csv
       +--*.csv
    +--data_test1_blank
       +--subj8log456.csv
       +--subj8log457.csv
       +--*.csv
    +--data_test2_blank
       +--subj1log1.csv
       +--subj1log2.csv
       +--*.csv
```


Please, look at main.ipynb for more detailed description.

### How to run
```python
main.ipynb: Cell -> Run All
```


The result would be in A_sbm0.ans, B_sbm0.ans, C_sbm0.ans files.